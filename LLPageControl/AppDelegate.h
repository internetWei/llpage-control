//
//  AppDelegate.h
//  LLPageControl
//
//  Created by LL on 2022/2/8.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (nonatomic, strong) UIWindow *window;

@end

