//
//  ViewController.m
//  LLPageControl
//
//  Created by LL on 2022/2/8.
//

#import "ViewController.h"

#import "LLPageControl.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.navigationItem.title = @"One View";
    self.view.backgroundColor = UIColor.whiteColor;
    
    LLPageControl *pageControl = [LLPageControl pageControlWithRadius:5.0 spacing:5.0 numberOfPages:10];
    pageControl.backgroundColor = UIColor.redColor;
    pageControl.frame = CGRectMake(0, 0, 150, 30);
    pageControl.center = self.view.center;
    [self.view addSubview:pageControl];
}

@end
